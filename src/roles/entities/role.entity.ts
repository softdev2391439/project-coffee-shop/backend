import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
} from 'typeorm';

@Entity({ engine: 'InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci' })
export class Role {
  @PrimaryGeneratedColumn({
    name: 'ROLE_ID',
  })
  id: number;
  @Column({
    name: 'ROLE_NAME',
  })
  name: string;

  //created and updated
  @CreateDateColumn({
    name: 'ROLE_CREATED',
  })
  created: Date;
  @UpdateDateColumn({
    name: 'ROLE_UPDATED',
  })
  updated: Date;

  //Relationship
  @OneToMany(() => User, (user) => user.role)
  users: User[];
}
