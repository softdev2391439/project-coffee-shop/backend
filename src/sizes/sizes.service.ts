import { Injectable } from '@nestjs/common';
import { CreateSizeDto } from './dto/create-size.dto';
import { UpdateSizeDto } from './dto/update-size.dto';
import { Size } from './entities/size.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class SizesService {
  constructor(
    @InjectRepository(Size)
    private sizesRepository: Repository<Size>,
  ) {}

  create(createSizeDto: CreateSizeDto) {
    return this.sizesRepository.save(createSizeDto);
  }

  findAll() {
    return this.sizesRepository.find();
  }

  findOne(id: number) {
    return this.sizesRepository.findOneByOrFail({ id: id });
  }

  async update(id: number, updateSizeDto: UpdateSizeDto) {
    await this.sizesRepository.findOneByOrFail({ id });
    await this.sizesRepository.update(id, updateSizeDto);
    const updatedStockDetail = await this.sizesRepository.findOneBy({
      id,
    });
    return updatedStockDetail;
  }

  async remove(id: number) {
    const deleteStockDetail = await this.sizesRepository.findOneOrFail({
      where: { id },
    });
    await this.sizesRepository.remove(deleteStockDetail);

    return deleteStockDetail;
  }
}
