import { Module } from '@nestjs/common';
import { CheckMemberService } from './check-member.service';
import { CheckMemberController } from './check-member.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckMember } from './entities/check-member.entity';
import { Member } from 'src/members/entities/member.entity';
import { MembersModule } from 'src/members/members.module';

@Module({
  imports: [TypeOrmModule.forFeature([CheckMember, Member]), MembersModule],
  controllers: [CheckMemberController],
  providers: [CheckMemberService],
})
export class CheckMemberModule { }
