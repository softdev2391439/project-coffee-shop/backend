import { Test, TestingModule } from '@nestjs/testing';
import { CheckMemberController } from './check-member.controller';
import { CheckMemberService } from './check-member.service';

describe('CheckMemberController', () => {
  let controller: CheckMemberController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckMemberController],
      providers: [CheckMemberService],
    }).compile();

    controller = module.get<CheckMemberController>(CheckMemberController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
