import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CheckMemberService } from './check-member.service';
import { CreateCheckMemberDto } from './dto/create-check-member.dto';
import { UpdateCheckMemberDto } from './dto/update-check-member.dto';

@Controller('checkmembers')
export class CheckMemberController {
  constructor(private readonly checkMemberService: CheckMemberService) { }

  @Post()
  create(@Body() createCheckMemberDto: CreateCheckMemberDto) {
    return this.checkMemberService.create(createCheckMemberDto);
  }

  @Get()
  findAll() {
    return this.checkMemberService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkMemberService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCheckMemberDto: UpdateCheckMemberDto) {
    return this.checkMemberService.update(+id, updateCheckMemberDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkMemberService.remove(+id);
  }
}
