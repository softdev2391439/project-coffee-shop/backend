import { Injectable, UnauthorizedException } from '@nestjs/common';
import { CreateCheckMemberDto } from './dto/create-check-member.dto';
import { UpdateCheckMemberDto } from './dto/update-check-member.dto';
import { MembersService } from 'src/members/members.service';
import { InjectRepository } from '@nestjs/typeorm';
import { CheckMember } from './entities/check-member.entity';
import { Repository } from 'typeorm/repository/Repository';
import { Member } from 'src/members/entities/member.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class CheckMemberService {
  constructor(
    private membersService: MembersService,
    @InjectRepository(CheckMember)
    private checkMemberRepository: Repository<CheckMember>,

    @InjectRepository(Member)
    private memberRepository: Repository<Member>,
  ) { }
  async create(createCheckMemberDto: CreateCheckMemberDto) {
    const member = await this.checkMember(createCheckMemberDto.email, createCheckMemberDto.password);
    const loginMember = new CheckMember();
    if (!member) {
      throw new Error('User not found');
    }
    loginMember.date = createCheckMemberDto.date;
    loginMember.timeIn = createCheckMemberDto.timeIn;
    loginMember.status = createCheckMemberDto.status;
    loginMember.totalHour = createCheckMemberDto.totalHour;
    loginMember.member = member

    return this.checkMemberRepository.save(loginMember);
  }

  findAll(): Promise<CheckMember[]> {
    return this.checkMemberRepository.find({
      relations: {
        member: true,
      },

    });
  }

  findOne(id: number) {
    return this.checkMemberRepository.findOneOrFail({
      where: { id },
      relations: { member: true },
    });
  }

  async update(id: number, updateCheckMemberDto: UpdateCheckMemberDto) {
    const checkmember = await this.checkMemberRepository.findOne({ where: { id } });
    if (!checkmember) {
      throw new Error('CheckInOut not found');
    }
    if (updateCheckMemberDto.timeOut) {
      checkmember.timeOut = new Date(); // Set current time as timeOut
      checkmember.totalHour = Math.floor((checkmember.timeOut.getTime() - checkmember.timeIn.getTime()) / (1000 * 60 * 60));
    }
    if (updateCheckMemberDto.status) {
      checkmember.status = updateCheckMemberDto.status; // Update status if provided
    }
    await this.checkMemberRepository.save(checkmember);
    return checkmember;
  }

  async remove(id: number) {
    const deleteCheckMember = await this.checkMemberRepository.findOne({ where: { id } });
    if (!deleteCheckMember) {
      throw new Error('CheckInOut not found');
    }
    return this.checkMemberRepository.remove(deleteCheckMember);
  }

  async checkMember(email: string, pass: string): Promise<any> {
    try {
      const member = await this.membersService.findOneByEmail(email);
      const isMatch = await bcrypt.compare(pass, member?.password);
      if (!isMatch) {
        throw new UnauthorizedException();
      }
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      return member;
    } catch (e) {
      console.log(e)
    }
  }
}
