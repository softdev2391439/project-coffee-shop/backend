import { PartialType } from '@nestjs/swagger';
import { CreateCheckMemberDto } from './create-check-member.dto';

export class UpdateCheckMemberDto extends PartialType(CreateCheckMemberDto) {}
