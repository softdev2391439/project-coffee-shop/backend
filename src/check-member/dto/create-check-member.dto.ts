import { Member } from "src/members/entities/member.entity"

export class CreateCheckMemberDto {
    email: string
    password: string
    date: Date
    timeIn: Date
    timeOut: Date
    status: string
    totalHour: number
    member: Member
}
