import { Member } from "src/members/entities/member.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm"

@Entity({ engine: 'InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci' })
export class CheckMember {
    @PrimaryGeneratedColumn({
        name: 'CHECKMEMBER_ID'
    })
    id: number;
    @Column({
        name: 'CHECKMEMBER_TIMEIN'
    })
    timeIn: Date;
    @Column({
        name: 'CHECKMEMBER_TIMEOUT',
        nullable: true,
        default: () => 'NULL',
    })
    timeOut?: Date | null;
    @Column({
        name: 'CHECKMEMBER_STATUS'
    })
    status: string;
    @Column({
        name: 'CHECKMEMBER_TOTALHOUR'
    })
    totalHour: number;
    @Column({
        name: 'CHECKMEMBER_DATE'
    })
    date: Date;

    @ManyToOne(() => Member, (member) => member.checkmember)
    member: Member;
}
