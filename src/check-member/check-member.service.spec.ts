import { Test, TestingModule } from '@nestjs/testing';
import { CheckMemberService } from './check-member.service';

describe('CheckMemberService', () => {
  let service: CheckMemberService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CheckMemberService],
    }).compile();

    service = module.get<CheckMemberService>(CheckMemberService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
