import { AuthModule } from 'src/auth/auth.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { User } from 'src/users/entities/user.entity';
import { UsersModule } from 'src/users/users.module';
import { Role } from 'src/roles/entities/role.entity';
import { RolesModule } from 'src/roles/roles.module';
import { DataSource } from 'typeorm';
import { Branch } from 'src/branches/entities/branch.entity';
import { BranchesModule } from 'src/branches/branches.module';
import { SalarysModule } from 'src/salarys/salarys.module';
import { Salary } from 'src/salarys/entities/salary.entity';
import { CheckInOut } from 'src/check-in-out/entities/check-in-out.entity';
import { CheckInOutModule } from 'src/check-in-out/check-in-out.module';
import { Member } from 'src/members/entities/member.entity';
import { MembersModule } from 'src/members/members.module';
import { ProductsModule } from 'src/products/products.module';
import { SubCategorysModule } from 'src/sub-categorys/sub-categorys.module';
import { SizesModule } from 'src/sizes/sizes.module';
import { CategoryModule } from 'src/category/category.module';
import { Product } from 'src/products/entities/product.entity';
import { SubCategory } from 'src/sub-categorys/entities/sub-category.entity';
import { Size } from 'src/sizes/entities/size.entity';
import { Category } from 'src/category/entities/category.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import { ReceiptDetail } from 'src/receipt/entities/receiptDetail.entity';
import { ReceiptModule } from 'src/receipt/receipt.module';
import { Promotion } from 'src/promotion/entities/promotion.entity';
import { PromotionModule } from 'src/promotion/promotion.module';
import { Stock } from 'src/stocks/entities/stock.entity';
import { StockDetail } from 'src/stockDetails/entities/stockDetail.entity';
import { Material } from 'src/material/entities/material.entity';
import { stocksModule } from 'src/stocks/stock.module';
import { stockDetailsModule } from 'src/stockDetails/stockDetail.module';
import { MaterialModule } from 'src/material/material.module';
import { UtilityCostModule } from 'src/utility-cost/utility-cost.module';
import { UtilityCost } from 'src/utility-cost/entities/utility-cost.entity';
import { UtilityDetail } from 'src/utility-cost/entities/uitlity-datail';
import { CheckMember } from 'src/check-member/entities/check-member.entity';
import { CheckMemberModule } from 'src/check-member/check-member.module';

@Module({
  imports: [
    //for SQLite
    // TypeOrmModule.forRoot({
    //   type: 'sqlite',
    //   database: 'database.sqlite',
    //   synchronize: true,
    //   entities: [
    //     Stock,
    //     User,
    //     StockDetail,
    //     Role,
    //     Branch,
    //     Material,
    //     Product,
    //     Category,
    //     SubCategory,
    //     Size,
    //     Customer,
    //     Promotion,
    //   ], //entityที่จะใช้
    // }),

    // For Mysql
    // need XAMPP (MySQL)
    // don't have to create User Account in PhpMyAdmin
    // create database name dcoffee in MySQL
    // in first time to run set synchronize = true
    // case have tables then set synchronize = false if not you will get  error

    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'angsila.informatics.buu.ac.th',
      port: 3306,
      username: 'cscamp11',
      password: 'vrIR6cv0bx',
      database: 'cscamp11',
      timezone: 'Asia/Bangkok',
      charset: 'utf8mb4_unicode_ci',
      entities: [
        User,
        Role,
        Branch,
        Salary,
        CheckInOut,
        Member,
        Product,
        SubCategory,
        Size,
        Category,
        Receipt,
        ReceiptDetail,
        Salary,
        Promotion,
        Stock,
        StockDetail,
        Material,
        UtilityCost,
        UtilityDetail,
        CheckMember,
      ],
      synchronize: false,
      keepConnectionAlive: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', '..', 'public'),
    }),
    UsersModule,
    AuthModule,
    RolesModule,
    BranchesModule,
    SalarysModule,
    CheckInOutModule,
    MembersModule,
    ProductsModule,
    SubCategorysModule,
    SizesModule,
    CategoryModule,
    ReceiptModule,
    SalarysModule,
    PromotionModule,
    stocksModule,
    stockDetailsModule,
    MaterialModule,
    UtilityCostModule,
    CheckMemberModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
