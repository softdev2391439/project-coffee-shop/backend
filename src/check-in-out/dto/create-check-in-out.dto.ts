import { User } from 'src/users/entities/user.entity';

export class CreateCheckInOutDto {
  email: string;
  password: string;
  date: Date;
  timeIn: Date;
  timeOut: Date;
  status: string;
  totalHour: number;
  user: User;
}
