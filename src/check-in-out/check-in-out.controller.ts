import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CheckInOutService } from './check-in-out.service';
import { CreateCheckInOutDto } from './dto/create-check-in-out.dto';
import { UpdateCheckInOutDto } from './dto/update-check-in-out.dto';


@Controller('checkInOuts')
export class CheckInOutController {
  constructor(private readonly checkInOutService: CheckInOutService) { }

  @Post()
  create(@Body() createCheckInOutDto: CreateCheckInOutDto) {
    return this.checkInOutService.create(createCheckInOutDto);
  }

  @Get()
  findAll() {
    return this.checkInOutService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkInOutService.findOne(+id);
  }


  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCheckInOutDto: UpdateCheckInOutDto,
  ) {
    return this.checkInOutService.update(+id, updateCheckInOutDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkInOutService.remove(+id);
  }
  @Get('filter/:id')
  async findAllByFilter(
    @Param('id') id: number,
  ) {
    try {
      const result = await this.checkInOutService.findByFilter(id);
      return result;
    } catch (error) {
      console.error("Error executing findByFilter:", error);
      throw error;
    }
  }

  @Get('filter/month/:month/:year')
  async findAllByMonth(
    @Param('month') month: number,
    @Param('year') year: number,
  ) {
    try {
      // เรียกใช้งาน findByMonth ใน CheckInOutService
      const result = await this.checkInOutService.findByMonth(month, year);
      return result;
    } catch (error) {
      console.error("Error executing findAllByMonth:", error);
      throw error;
    }
  }

}
