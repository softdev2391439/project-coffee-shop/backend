import { Salary } from "src/salarys/entities/salary.entity";
import { User } from "src/users/entities/user.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm"

@Entity({ engine: 'InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci' })
export class CheckInOut {
  @PrimaryGeneratedColumn({
    name: 'CHECKINOUT_ID',
  })
  id: number;
  @Column({
    name: 'CHECKINOUT_TIMEIN',
  })
  timeIn: Date;
  @Column({
    name: 'CHECKINOUT_TIMEOUT',
    nullable: true,
    default: () => 'NULL',
  })
  timeOut?: Date | null;
  @Column({
    name: 'CHECKINOUT_STATUS',
  })
  status: string;
  @Column({
    name: 'CHECKINOUT_TOTALHOUR',
  })
  totalHour: number;
  @Column({
    name: 'CHECKINOUT_DATE',
  })
  date: Date;

  @ManyToOne(() => User, (user) => user.checkInOut)
  @JoinColumn({ name: 'EMP_ID' })
  user: User;

  @ManyToOne(() => Salary, (salary) => salary.checkInOuts)
  @JoinColumn({ name: 'SLR_ID' })
  salary: Salary;
}
