import { Module } from '@nestjs/common';
import { CheckInOutService } from './check-in-out.service';
import { CheckInOutController } from './check-in-out.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckInOut } from './entities/check-in-out.entity';
import { User } from 'src/users/entities/user.entity';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [TypeOrmModule.forFeature([CheckInOut, User]), UsersModule],
  controllers: [CheckInOutController],
  providers: [CheckInOutService],
})
export class CheckInOutModule {}
