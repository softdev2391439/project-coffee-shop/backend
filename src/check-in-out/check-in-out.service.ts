import { Injectable, UnauthorizedException } from '@nestjs/common';
import { CreateCheckInOutDto } from './dto/create-check-in-out.dto';
import { UpdateCheckInOutDto } from './dto/update-check-in-out.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CheckInOut } from './entities/check-in-out.entity';
import { Repository } from 'typeorm/repository/Repository';
import { User } from 'src/users/entities/user.entity';
import * as bcrypt from 'bcrypt';
import { UsersService } from '../users/users.service';

@Injectable()
export class CheckInOutService {
  constructor(
    private usersService: UsersService,
    @InjectRepository(CheckInOut)
    private checkInOutRepository: Repository<CheckInOut>,

    @InjectRepository(User) private UserRepository: Repository<User>,
  ) { }
  async create(createCheckInOutDto: CreateCheckInOutDto) {
    const user = await this.checkUser(
      createCheckInOutDto.email,
      createCheckInOutDto.password,
    );
    const loginUser = new CheckInOut();
    if (!user) {
      throw new Error('User not found');
    }
    loginUser.date = createCheckInOutDto.date;
    loginUser.timeIn = createCheckInOutDto.timeIn;
    loginUser.status = createCheckInOutDto.status;
    loginUser.totalHour = createCheckInOutDto.totalHour;
    loginUser.user = user;
    console.log(JSON.stringify(loginUser));

    return this.checkInOutRepository.save(loginUser);
  }

  findAll(): Promise<CheckInOut[]> {
    return this.checkInOutRepository.find({
      relations: {
        user: true,
      },
    });
  }

  findOne(id: number) {
    return this.checkInOutRepository.findOneOrFail({
      where: { id },
      relations: { user: true },
    });
  }

  // findAllByEmailPassword(userId: number) {
  //   return this.checkInOutRepository.find({
  //     where: { userId: userId },
  //     relations: { user: true },
  //   });
  // }

  async update(id: number, updateCheckInOutDto: UpdateCheckInOutDto) {
    const checkInOut = await this.checkInOutRepository.findOne({
      where: { id },
    });
    if (!checkInOut) {
      throw new Error('CheckInOut not found');
    }
    if (updateCheckInOutDto.timeOut) {
      checkInOut.timeOut = new Date(); // Set current time as timeOut
      checkInOut.totalHour = Math.floor(
        (checkInOut.timeOut.getTime() - checkInOut.timeIn.getTime()) /
        (1000 * 60 * 60),
      );
    }
    if (updateCheckInOutDto.status) {
      checkInOut.status = updateCheckInOutDto.status; // Update status if provided
    }
    await this.checkInOutRepository.save(checkInOut);
    return checkInOut;
  }

  async remove(id: number) {
    const deleteCheckInOut = await this.checkInOutRepository.findOne({
      where: { id },
    });
    if (!deleteCheckInOut) {
      throw new Error('CheckInOut not found');
    }
    return this.checkInOutRepository.remove(deleteCheckInOut);
  }

  async checkUser(email: string, pass: string): Promise<any> {
    try {
      const user = await this.usersService.findOneByEmail(email);
      console.log(user);
      console.log(pass);
      const isMatch = await bcrypt.compare(pass, user?.password);
      if (!isMatch) {
        console.log('not matching');
        throw new UnauthorizedException();
      }
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      return user;
    } catch (e) {
      console.log(e);
    }
  }

  async findByFilter(id: number) {
    try {
      const result = await this.checkInOutRepository.query(`CALL 	CalculateSalary(?)`,
        [id]);
      const A = result[0];
      return A;
    } catch (error) {
      console.error("Error executing findByFilter:", error);
      throw error;
    }
  }

  async findByMonth(month: number, year: number) {
    try {
      const result = await this.checkInOutRepository.query('CALL GetCheckInOutByMonth(?, ?)', [month, year]);
      const checkInOutData = result[0];
      return checkInOutData;
    } catch (error) {
      console.error("Error executing findByMonth:", error);
      throw error;
    }
  }

}
