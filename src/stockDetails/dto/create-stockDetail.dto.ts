import { Double } from 'typeorm';

export class CreateStockDetailDto {
  SD_QOH: Double;
  SD_BALANCE: Double;
  SD_MATERIAL: number;
}
