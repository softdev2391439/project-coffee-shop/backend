import { Module } from '@nestjs/common';
import { StockDetailService } from './stockDetail.service';
import { StockDetailController } from './stockDetail.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StockDetail } from './entities/stockDetail.entity';
import { Stock } from 'src/stocks/entities/stock.entity';
import { Material } from 'src/material/entities/material.entity';

@Module({
  imports: [TypeOrmModule.forFeature([StockDetail, Stock, Material])],
  controllers: [StockDetailController],
  providers: [StockDetailService],
  exports: [StockDetailService],
})
export class stockDetailsModule {}
