export class CreateReceiptDto {
  receiptDetails: {
    productId: number;
    qty: number;
  }[];
  totalPrice: number;
  discount: number;
  income: number;
  change: number;
  paymentType: string;
  userId: number;
  branchId: number;
  memberId: number;
  promotionId: number;
}
