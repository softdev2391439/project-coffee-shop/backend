import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ReceiptDetail } from './receiptDetail.entity';
import { Member } from 'src/members/entities/member.entity';
import { Branch } from 'src/branches/entities/branch.entity';
import { User } from 'src/users/entities/user.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';


@Entity({ engine: 'InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci' })
export class Receipt {
  @PrimaryGeneratedColumn({
    name: 'RECEIPT_ID',
  })
  id: number;

  @Column({
    name: 'RECEIPT_QTY',
  })
  qty: number;

  @Column({
    name: 'RECEIPT_TOTALPRICE',
  })
  totalPrice: number;

  @Column({
    name: 'RECEIPT_DISCOUNT',
  })
  discount: number;

  @Column({
    name: 'RECEIPT_PAYMENTTYPE',
  })
  paymentType: string;

  @Column({
    name: 'RECEIPT_INCOME',
  })
  income: number;

  @Column({
    name: 'RECEIPT_CHANGE',
  })
  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updated: Date;

  RECEIPT_CREATEDDATE: Date;

  @UpdateDateColumn()
  RECEIPT_UPDATEDDATE: Date;

  @OneToMany(() => ReceiptDetail, (receiptDetails) => receiptDetails.receipt)
  receiptDetails: ReceiptDetail[];

  @ManyToOne(() => Member, (member) => member.receipt)
  member: Member;

  @ManyToOne(() => Branch, (branch) => branch.receipt)
  branch: Branch;

  @ManyToOne(() => User, (user) => user.receipts)
  user: User;

  @ManyToOne(() => Promotion, (promotion) => promotion.receipts)
  promotion: Promotion;
}
