import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Receipt } from './receipt.entity';
import { Product } from 'src/products/entities/product.entity';

@Entity()
export class ReceiptDetail {
  @PrimaryGeneratedColumn({
    name: 'RECEIPT_DT_ID',
  })
  id: number;

  @Column({
    name: 'RECEIPT_DT_NAME',
  })
  name: string;

  @Column({
    name: 'RECEIPT_DT_QTY',
  })
  qty: number;

  @Column({
    name: 'RECEIPT_DT_PRICE',
  })
  price: number;

  @Column({
    name: 'RECEIPT_DT_TOTAL',
  })
  total: number;

  @ManyToOne(() => Receipt, (receipt) => receipt.receiptDetails, {
    onDelete: 'CASCADE',
  })
  receipt: Receipt;

  @ManyToOne(() => Product, (product) => product.receiptDetails)
  product: Product;
}
