import { Module } from '@nestjs/common';
import { ReceiptService } from './receipt.service';
import { ReceiptController } from './receipt.controller';
import { Receipt } from './entities/receipt.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReceiptDetail } from './entities/receiptDetail.entity';
import { User } from 'src/users/entities/user.entity';
import { Member } from 'src/members/entities/member.entity';
import { Branch } from 'src/branches/entities/branch.entity';
import { Product } from 'src/products/entities/product.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';
// import { Promotion } from 'src/promotions/entities/promotion.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Receipt,
      ReceiptDetail,
      User,
      Member,
      Branch,
      Product,
      Promotion,
    ]),
  ],
  controllers: [ReceiptController],
  providers: [ReceiptService],
})
export class ReceiptModule {}
