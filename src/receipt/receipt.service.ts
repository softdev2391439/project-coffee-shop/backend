import { Injectable } from '@nestjs/common';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { Repository } from 'typeorm';
import { ReceiptDetail } from './entities/receiptDetail.entity';
import { User } from 'src/users/entities/user.entity';
import { Product } from 'src/products/entities/product.entity';
import { Member } from 'src/members/entities/member.entity';
import { Branch } from 'src/branches/entities/branch.entity';

@Injectable()
export class ReceiptService {
  constructor(
    @InjectRepository(Receipt) private receiptsRepository: Repository<Receipt>,
    @InjectRepository(ReceiptDetail)
    private receiptDetailsRepository: Repository<ReceiptDetail>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Product) private productsRepository: Repository<Product>,
    @InjectRepository(Member) private membersRepository: Repository<Member>,
    @InjectRepository(Branch) private branchsRepository: Repository<Branch>,
    // @InjectRepository(Promotion) private promotionsRepository: Repository<Promotion>,
  ) { }

  async create(createReceiptDto: CreateReceiptDto) {
    const receipt = new Receipt();
    const user = await this.usersRepository.findOneBy({
      id: createReceiptDto.userId,
    });
    const branch = await this.branchsRepository.findOneBy({
      id: createReceiptDto.branchId,
    });
    const member = await this.membersRepository.findOneBy({
      id: createReceiptDto.memberId,
    });

    receipt.totalPrice = createReceiptDto.totalPrice;
    receipt.discount = createReceiptDto.discount;
    receipt.paymentType = createReceiptDto.paymentType;
    receipt.income = createReceiptDto.income;

    receipt.qty = 0;
    receipt.user = user;
    receipt.branch = branch;
    receipt.member = member;


    receipt.receiptDetails = [];
    console.log(receipt);
    console.log(createReceiptDto.receiptDetails);
    for (const rd of createReceiptDto.receiptDetails) {
      const receiptDetail = new ReceiptDetail();
      receiptDetail.product = await this.productsRepository.findOneBy({
        id: rd.productId,
      });
      receiptDetail.name = receiptDetail.product.name;
      receiptDetail.price = receiptDetail.product.price;
      receiptDetail.qty = rd.qty;
      receiptDetail.total = receiptDetail.price * receiptDetail.qty;
      await this.receiptDetailsRepository.save(receiptDetail);
      receipt.receiptDetails.push(receiptDetail);

      receipt.qty += receiptDetail.qty;
    }
    return this.receiptsRepository.save(receipt);
  }

  findAll() {
    return this.receiptsRepository.find({
      relations: {
        receiptDetails: true,
        user: true,
        branch: true,
        member: true,

      },
      order: {
        id: 'desc',
      },
    });
  }

  findOne(id: number) {
    return this.receiptsRepository.findOneOrFail({
      where: { id },
      relations: { receiptDetails: true },
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  update(id: number, updateReceiptDto: UpdateReceiptDto) {
    return `This action updates a #${id} receipt`;
  }

  async findByBranch(branchId: number) {
    return await this.receiptsRepository.find({
      where: { branch: { id: branchId } },
    });
  }

  async remove(id: number) {
    const deleteReceipt = await this.receiptsRepository.findOneOrFail({
      where: { id },
    });
    await this.receiptsRepository.remove(deleteReceipt);

    return deleteReceipt;
  }
}
