import { Test, TestingModule } from '@nestjs/testing';
import { SubCategorysController } from './sub-categorys.controller';
import { SubCategorysService } from './sub-categorys.service';

describe('SubCategorysController', () => {
  let controller: SubCategorysController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SubCategorysController],
      providers: [SubCategorysService],
    }).compile();

    controller = module.get<SubCategorysController>(SubCategorysController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
