import { Test, TestingModule } from '@nestjs/testing';
import { SubCategorysService } from './sub-categorys.service';

describe('SubCategorysService', () => {
  let service: SubCategorysService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SubCategorysService],
    }).compile();

    service = module.get<SubCategorysService>(SubCategorysService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
