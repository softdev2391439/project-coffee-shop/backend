import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { Promotion } from './entities/promotion.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class PromotionService {
  constructor(
    @InjectRepository(Promotion)
    private promotionRepository: Repository<Promotion>,
  ) { }
  async create(createPromotionDto: CreatePromotionDto): Promise<Promotion> {
    const promotion = new Promotion();
    promotion.name = createPromotionDto.name;
    promotion.discount = createPromotionDto.discount;
    promotion.startDate = createPromotionDto.startDate;
    promotion.endDate = createPromotionDto.endDate;
    promotion.details = createPromotionDto.details;
    promotion.category = JSON.parse(
      JSON.stringify(createPromotionDto.category),
    );
    promotion.pointUse = createPromotionDto.pointUse;
    promotion.status = createPromotionDto.status;
    return await this.promotionRepository.save(promotion);
  }

  findAll(): Promise<Promotion[]> {
    return this.promotionRepository.find({
      relations: { category: true },
    });
  }

  findOne(id: number) {
    return this.promotionRepository.findOneOrFail({
      where: { id: id },
      relations: { category: true },
    });
  }

  findByCategory(categoryId: number) {
    return this.promotionRepository.find({
      where: { category: { id: categoryId } },
      relations: { category: true },
    });
  }

  async update(id: number, updatePromotionDto: UpdatePromotionDto) {
    const promotion = new Promotion();
    promotion.name = updatePromotionDto.name;
    promotion.discount = updatePromotionDto.discount;
    promotion.startDate = updatePromotionDto.startDate;
    promotion.endDate = updatePromotionDto.endDate;
    promotion.details = updatePromotionDto.details;
    promotion.category = JSON.parse(
      JSON.stringify(updatePromotionDto.category),
    );
    promotion.pointUse = updatePromotionDto.pointUse;
    promotion.status = updatePromotionDto.status;
    const updatePromotion = await this.promotionRepository.findOneOrFail({
      where: { id },
      relations: { category: true },
    });
    updatePromotion.name = promotion.name;
    updatePromotion.discount = promotion.discount;
    updatePromotion.startDate = promotion.startDate;
    updatePromotion.endDate = promotion.endDate;
    updatePromotion.details = promotion.details;
    updatePromotion.category = promotion.category;
    updatePromotion.pointUse = promotion.pointUse;
    updatePromotion.status = promotion.status;

    await this.promotionRepository.save(updatePromotion);
    const result = await this.promotionRepository.findOne({
      where: { id },
      relations: { category: true },
    });
    return result;
  }

  async remove(id: number) {
    const deletePromotion = await this.promotionRepository.findOneOrFail({
      where: { id },
    });
    await this.promotionRepository.remove(deletePromotion);

    return deletePromotion;
  }
}