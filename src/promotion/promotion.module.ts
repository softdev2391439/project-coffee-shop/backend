import { Module } from '@nestjs/common';
import { PromotionService } from './promotion.service';
import { PromotionController } from './promotion.controller';
import { Promotion } from './entities/promotion.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import { Category } from 'src/category/entities/category.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Promotion, Receipt, Category])],
  controllers: [PromotionController],
  providers: [PromotionService],
  exports: [TypeOrmModule],
})
export class PromotionModule {}
