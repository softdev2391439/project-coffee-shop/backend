import { Category } from 'src/category/entities/category.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity({ engine: 'InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci' })
export class Promotion {
  @PrimaryGeneratedColumn({
    name: 'PRO_ID',
  })
  id: number;

  @Column({
    name: 'PRO_NAME',
  })
  name: string;

  @Column({
    name: 'PRO_DETAILS',
  })
  details: string;

  @Column({
    name: 'PRO_START_DATE',
  })
  startDate: Date;

  @Column({
    name: 'PRO_END_DATE',
  })
  endDate: Date;

  @Column({
    name: 'PRO_DISCOUNT',
  })
  discount: number;

  @Column({
    name: 'PRO_STATUS',
  })
  status: string;

  @Column({
    name: 'PRO_POINT_USE',
  })
  pointUse: number;

  @OneToMany(() => Receipt, (receipt) => receipt.promotion)
  receipts: Receipt[];

  @ManyToOne(() => Category, category => category.promotions)
  category: Category;
}
