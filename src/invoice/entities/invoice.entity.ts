import { Branch } from 'src/branches/entities/branch.entity';
import { InvoiceDetail } from 'src/invoice-detail/entities/invoice-detail.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Double,
  Entity,
  JoinTable,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Invoice {
  @PrimaryGeneratedColumn()
  INV_ID: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Branch)
  @JoinTable({ name: 'BRANCH_ID' })
  INV_BRANCH: Branch;

  @ManyToOne(() => User)
  @JoinTable({ name: 'EMP_ID' })
  INV_USER: User;

  @Column()
  INV_PLACE: string;

  @Column({ type: 'real' })
  INV_PRICE: Double;

  @OneToMany(() => InvoiceDetail, (InvoiceDetail) => InvoiceDetail.IND_INVOICE)
  INV_INVOICEDETAILS: InvoiceDetail[];
}
