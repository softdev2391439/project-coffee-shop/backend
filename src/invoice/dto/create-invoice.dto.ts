import { Double } from 'typeorm';

export class CreateInvoiceDto {
  INV_BRANCH: number;
  INV_USER: number;
  INV_PLACE: string;
  INV_PRICE: Double;
  INV_INVOICEDETAILS: string;
}
