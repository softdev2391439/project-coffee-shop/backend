import { Injectable } from '@nestjs/common';
import { CreateInvoiceDto } from './dto/create-invoice.dto';
import { UpdateInvoiceDto } from './dto/update-invoice.dto';
import { Invoice } from './entities/invoice.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { InvoiceDetail } from 'src/invoice-detail/entities/invoice-detail.entity';
import { User } from 'src/users/entities/user.entity';
import { Branch } from 'src/branches/entities/branch.entity';

@Injectable()
export class InvoiceService {
  constructor(
    @InjectRepository(Invoice) private invoicesRepository: Repository<Invoice>,
    @InjectRepository(InvoiceDetail)
    private invoiceDetailsRepository: Repository<InvoiceDetail>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Branch)
    private branchsRepository: Repository<Branch>,
  ) { }
  async create(createInvoiceDto: CreateInvoiceDto) {
    const invoice = new Invoice();
    try {
      const invoiceDetails: InvoiceDetail[] = JSON.parse(
        JSON.stringify(createInvoiceDto.INV_INVOICEDETAILS)
      );
      for (const invoiceDetail of invoiceDetails) {
        if (invoiceDetail.IND_ID > -1) {
          await this.invoiceDetailsRepository.update(
            invoiceDetail.IND_ID,
            invoiceDetail,
          );
        } else {
          await this.invoiceDetailsRepository.save(invoiceDetail);
        }
      }
      invoice.INV_INVOICEDETAILS = invoiceDetails;
      const user = await this.usersRepository.findOneOrFail({
        where: { id: createInvoiceDto.INV_USER },
      });
      invoice.INV_USER = user;
      const branch = await this.branchsRepository.findOneOrFail({
        where: { id: createInvoiceDto.INV_BRANCH },
      });
      invoice.INV_BRANCH = branch;
      invoice.INV_PLACE = createInvoiceDto.INV_PLACE
      invoice.INV_PRICE = createInvoiceDto.INV_PRICE
      return this.invoicesRepository.save(invoice);
    } catch (error) {
      console.error('Error parsing invoice details:', error);
    }
  }

  findAll() {
    return this.invoicesRepository.find({ relations: { INV_BRANCH: true, INV_USER: true, INV_INVOICEDETAILS: true } });
  }

  findOne(id: number) {
    return `This action returns a #${id} invoice`;
  }

  update(id: number, updateInvoiceDto: UpdateInvoiceDto) {
    return `This action updates a #${updateInvoiceDto} invoice number ${id}`;
  }

  remove(id: number) {
    return `This action removes a #${id} invoice`;
  }
}
