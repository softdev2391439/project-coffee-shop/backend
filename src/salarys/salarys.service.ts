import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { Salary } from './entities/salary.entity';
import { UpdateSalaryDto } from './dto/update-salary.dto';


@Injectable()
export class SalarysService {
  constructor(
    @InjectRepository(Salary) private salarysRepository: Repository<Salary>,
  ) {}

  async create(createSalaryDto: CreateSalaryDto): Promise<Salary> {
    return this.salarysRepository.save(createSalaryDto);
  }

  findAll() {
    return this.salarysRepository.find({
      relations: {
        branch: true,
        user: { role: true },
      },
    });
  }
  findOne(id: number) {
    return this.salarysRepository.findOne({
      where: { id },
      relations: { branch: true, user: true },
    });
  }

  async findByBranch(branchId: number) {
    return await this.salarysRepository.find({
      relations: { branch: true, user: true },
      where: { branch: { id: branchId } },
    });
  }

  async update(
    id: number,
    updateSalaryDto: UpdateSalaryDto,
  ): Promise<Salary | undefined> {
    await this.salarysRepository.update(id, updateSalaryDto);
    return this.salarysRepository.findOne({
      where: { id },
      relations: { branch: true, user: true },
    });
  }

  async remove(id: number) {
    const removesalary = await this.salarysRepository.findOneBy({ id });
    return this.salarysRepository.remove(removesalary);
  }

  async getTotalPay() {
    const result = await this.salarysRepository.query(
      'CALL GetMaxTotalPayment();',
    );
    return result[0];
  }

  async getTotalPayByMonth() {
    const result = await this.salarysRepository.query(
      'CALL GetMaxTotalPaymentByMonth();',
    );
    return result;
  }
}
