import { Branch } from 'src/branches/entities/branch.entity';
import { CheckInOut } from 'src/check-in-out/entities/check-in-out.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ engine: 'InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci' })
export class Salary {
  @PrimaryGeneratedColumn({
    name: 'SLR_ID',
  })
  id: number;

  @Column({
    name: 'SLR_MONTH',
  })
  month: number;

  @Column({
    name: 'SLR_PRICEHOUR',
  })
  pricePerHour: number;

  @Column({
    name: 'SLR_TOTALPAY',
    nullable: true,
  })
  totalPay: number;

  @Column({
    name: 'SLR_TOTALHOUR',
    nullable: true,
  })
  totalHour: number;

  @Column({
    name: 'SLR_STATUS',
    default: 'not pay',
  })
  status: string;

  @Column({
    name: 'SLR_PAYDATE',
    nullable: true,
  })
  payDate: Date;

  @CreateDateColumn({
    name: 'SLR_CREATED',
  })
  created: Date;

  @UpdateDateColumn({
    name: 'SLR_UPDATED',
  })
  updated: Date;

  @ManyToOne(() => User, (user) => user.salarys)
  @JoinColumn({ name: 'EMP_ID' })
  user: User;

  @ManyToOne(() => Branch, (branch) => branch.salarys)
  @JoinColumn({ name: 'BRANCH_ID' })
  branch: Branch;

  @OneToMany(() => CheckInOut, (checkInOut) => checkInOut.user)
  checkInOuts: CheckInOut[];
}
