import { User } from 'src/users/entities/user.entity';

export class CreateSalaryDto {
  pricePerHour: number;
  status: string;
  totalPay: number;
  totalHour: number;
  payDate: Date;
  month: number;
  user: User;
}
