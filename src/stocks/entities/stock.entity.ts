import { Branch } from 'src/branches/entities/branch.entity';
import { StockDetail } from 'src/stockDetails/entities/stockDetail.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
  JoinTable,
} from 'typeorm';
@Entity({ engine: 'InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci' })
export class Stock {
  @PrimaryGeneratedColumn()
  S_ID: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => StockDetail, (stockDetail) => stockDetail.SD_STOCK)
  S_STOCKDETAILS: StockDetail[];

  @ManyToOne(() => User, (user) => user.stocks)
  S_USER: User;

  @ManyToOne(() => Branch)
  @JoinTable({ name: 'BRANCH_ID' })
  S_BRANCH: Branch;
}
