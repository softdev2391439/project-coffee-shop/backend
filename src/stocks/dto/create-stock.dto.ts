export class CreateStockDto {
  stockDetails: string;
  userId: number;
  branchId: number;
}
