import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Member } from './entities/member.entity';
import { CreateMemberDto } from './dto/create-member.dto';
import { Repository } from 'typeorm';
import { UpdateMemberDto } from './dto/update-member.dto';

@Injectable()
export class MembersService {
  constructor(
    @InjectRepository(Member)
    private memberRepository: Repository<Member>,
  ) { }

  create(createMemberDto: CreateMemberDto): Promise<Member> {
    return this.memberRepository.save(createMemberDto);
  }

  findAll(): Promise<Member[]> {
    return this.memberRepository.find();
  }

  findOne(id: number) {
    return this.memberRepository.findOneBy({ id: id });
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    await this.memberRepository.update(id, updateMemberDto);
    const member = await this.memberRepository.findOneBy({ id });
    return member;
  }

  async remove(id: number) {
    const deleteMember = await this.memberRepository.findOneBy({ id });
    return this.memberRepository.remove(deleteMember);
  }

  findOneByEmail(email: string) {
    return this.memberRepository.findOneBy({ email: email });
  }

  async signIn(email: string, pass: string): Promise<any> {
    const member = await this.findOneByEmail(email);
    if (member && member.password === pass) {

    } else {
      throw new UnauthorizedException('Invalid email or password');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { password, ...result } = member;
    return result;
  }

  async updatePoints(id: number, newPoints: number): Promise<Member> {
    // Retrieve the member from the database
    const member = await this.memberRepository.findOneBy({ id: id });

    // Check if the member exists
    if (!member) {
      throw new Error(`Member with ID ${id} not found`);
    }

    // Update the member's points
    member.point = newPoints;

    // Save the updated member to the database
    await this.memberRepository.save(member);

    return member;
  }
}
