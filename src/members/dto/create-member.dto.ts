export class CreateMemberDto {
  fristName: string;
  lastName: string;
  email: string;
  tel: string;
  birth: Date;
  point: number;
  pointRate: number;
  indate: Date;
}
