import { PartialType } from '@nestjs/swagger';
import { CreateMemberDto } from './create-member.dto';

export class UpdateMemberDto extends PartialType(CreateMemberDto) {
  fristName: string;
  lastName: string;
  email: string;
  tel: string;
  birth: Date;
  point: number;
  pointRate: number;
  indate: Date;
}
