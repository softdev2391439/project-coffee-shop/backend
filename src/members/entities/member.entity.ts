import { CheckMember } from 'src/check-member/entities/check-member.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ engine: 'InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci' })
export class Member {
  @PrimaryGeneratedColumn({
    name: 'MEMBER_ID',
  })
  id: number;

  @Column({
    name: 'MEMBER_FNAME',
  })
  fristName: string;

  @Column({
    name: 'MEMBER_LNAME',
  })
  lastName: string;

  @Column({
    name: 'MEMBER_EMAIL',
  })
  email: string;
  @Column({
    name: 'MEMBER_PASSWORD',
  })
  password: string;

  @Column({
    name: 'MEMBER_TEL',
  })
  tel: string;

  @Column({
    name: 'MEMBER_BIRTH',
    type: 'date',
  })
  birth: Date;

  @Column({
    name: 'MEMBER_POINT',
  })
  point: number;

  @Column({
    name: 'MEMBER_POINTRATE',
  })
  pointRate: number;

  @Column({
    name: 'MEMBER_INDATE',
    type: 'date',
  })
  indate: Date;

  @OneToMany(() => Receipt, (receipt) => receipt.member)
  receipt: Receipt;

  @OneToMany(() => CheckMember, (checkmember) => checkmember.member)
  checkmember: CheckMember;
}
