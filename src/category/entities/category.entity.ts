import { Product } from 'src/products/entities/product.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ engine: 'InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci' })
export class Category {
  @PrimaryGeneratedColumn({
    name: 'CATEGORY_ID',
  })
  id: number;

  @Column({
    name: 'CATEGORY_NAME',
  })
  name: string;

  @CreateDateColumn({
    name: 'CATEGORY_CREATED',
  })
  created: Date;

  @UpdateDateColumn({
    name: 'CATEGORY_UPDATED',
  })
  updated: Date;

  @OneToMany(() => Product, (product) => product.category)
  @JoinColumn({ name: 'PRODUCT_ID' })
  products: Product[];

  @OneToMany(() => Promotion, (promotion) => promotion.category)
  @JoinColumn({ name: 'PROMOTION_ID' })
  promotions: Promotion[];
}
