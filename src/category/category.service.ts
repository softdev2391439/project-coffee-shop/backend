import { Injectable } from '@nestjs/common';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { Category } from './entities/category.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Category)
    private categorysRepository: Repository<Category>,
  ) {}

  create(createCategoryDto: CreateCategoryDto) {
    return this.categorysRepository.save(createCategoryDto);
  }

  findAll() {
    return this.categorysRepository.find();
  }

  findOne(id: number) {
    return this.categorysRepository.findOneByOrFail({ id: id });
  }

  async update(id: number, updateCategoryDto: UpdateCategoryDto) {
    await this.categorysRepository.findOneByOrFail({ id });
    await this.categorysRepository.update(id, updateCategoryDto);
    const updatedCategory = await this.categorysRepository.findOneBy({ id });
    return updatedCategory;
  }

  async remove(id: number) {
    const deletedCategory = await this.categorysRepository.findOneByOrFail({
      id,
    });
    await this.categorysRepository.remove(deletedCategory);
    return deletedCategory;
  }
}
