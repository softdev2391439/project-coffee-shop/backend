import { PartialType } from '@nestjs/mapped-types';
import { CreateUtilityCostDto } from './create-utility-cost.dto';
import { User } from 'src/users/entities/user.entity';
import { UtilityDetail } from '../entities/uitlity-datail';

export class UpdateUtilityCostDto extends PartialType(CreateUtilityCostDto) {
  totalPrice: number;
  totalItem: number;
  user: User;
  ucDetails: UtilityDetail[];
}
