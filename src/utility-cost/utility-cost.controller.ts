import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
} from '@nestjs/common';
import { UtilityCostService } from './utility-cost.service';
import { CreateUtilityCostDto } from './dto/create-utility-cost.dto';

@Controller('utility-cost')
export class UtilityCostController {
  constructor(private readonly utilityCostService: UtilityCostService) {}

  @Post()
  create(@Body() createUtilityCostDto: CreateUtilityCostDto) {
    return this.utilityCostService.create(createUtilityCostDto);
  }

  @Get()
  findAll() {
    return this.utilityCostService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.utilityCostService.findOne(+id);
  }

  @Get('getDtailByMonth/:year/:month/:branch')
  getDtailByMonth(@Param('year') year: number, @Param('month') month: number, @Param('branch') branch: string) {
    return this.utilityCostService.getDtailByMonth(branch, month, year);
  }

  @Get('getpriceGroupMonth/:branch/:year')
  getpriceGroupMonth(@Param('year') year: number, @Param('branch') branch: string) {
    return this.utilityCostService.getpriceGroupMonth(branch, year);
  }

  @Get('get/year')
  getYear() {
    return this.utilityCostService.getYear();
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.utilityCostService.remove(+id);
  }
}
