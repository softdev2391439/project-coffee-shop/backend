import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UtilityCost } from './utility-cost.entity';
@Entity({ engine: 'InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci' })
export class UtilityDetail {
  @PrimaryGeneratedColumn({
    name: 'UCD_ID',
  })
  id: number;

  @Column({
    name: 'UCD_TYPE',
  })
  type: string;

  @Column({
    name: 'UCD_PRICE',
    type: 'decimal',
    precision: 10,
    scale: 2,
  })
  price: number;

  @Column({
    name: 'UCD_DATE',
  })
  date: Date;

  @ManyToOne(() => UtilityCost, (utilityCost) => utilityCost.ucDetails)
  @JoinColumn({
    name: 'UC_ID',
  })
  utilityCost: UtilityCost;

  //Created and Updated
  @CreateDateColumn({
    name: 'UCD_CREATED',
  })
  created: Date;

  @UpdateDateColumn({
    name: 'UCD_UPDATED',
  })
  updated: Date;
}
