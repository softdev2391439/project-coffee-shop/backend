import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { unlink } from 'fs/promises';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) { }
  async create(createUserDto: CreateUserDto) {
    const user = new User();
    user.name = createUserDto.name;
    user.gender = createUserDto.gender;
    user.height = parseFloat(createUserDto.height);
    user.weight = parseFloat(createUserDto.weight);
    user.bloodType = createUserDto.bloodType;
    user.age = parseInt(createUserDto.age);
    user.birthDate = new Date(createUserDto.birthDate);
    user.phone = createUserDto.phone;
    user.email = createUserDto.email;
    user.address = createUserDto.address;
    user.role = JSON.parse(createUserDto.role);
    user.startDate = new Date(createUserDto.startDate);
    user.status = createUserDto.status;
    user.salary = parseFloat(createUserDto.salary);
    user.branch = JSON.parse(createUserDto.branch);
    //make password hash
    const saltOrRounds = 10;
    user.password = await bcrypt.hash(createUserDto.password, saltOrRounds);
    if (createUserDto.image && createUserDto.image !== '') {
      user.image = createUserDto.image;
    }
    return this.usersRepository.save(user);
  }

  findAll() {
    return this.usersRepository.find({ relations: ['role', 'branch'] });
  }

  async chagePassword(email: string, password: string) {
    const user = await this.usersRepository.findOne({ where: { email } });
    const hashPassword = await bcrypt.hash(password, 10);
    user.password = hashPassword;
    return this.usersRepository.save(user);
  }

  findAllByRole(roleId: number) {
    return this.usersRepository.find({
      relations: ['role', 'branch'],
      where: { role: { id: roleId } },
    });
  }

  async findByBranch(branchId: number) {
    return await this.usersRepository.find({
      relations: ['role', 'branch'],
      where: { branch: { id: branchId } },
    });
  }
  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      select: [
        'id',
        'name',
        'gender',
        'height',
        'weight',
        'bloodType',
        'age',
        'birthDate',
        'phone',
        'email',
        'address',
        'role',
        'startDate',
        'status',
        'salary',
        'branch',
        'image',
      ],
      relations: ['role', 'branch'],
    });
  }

  findOneByEmail(email: string) {
    return this.usersRepository.findOneOrFail({
      select: [
        'id',
        'name',
        'password',
        'gender',
        'height',
        'weight',
        'bloodType',
        'age',
        'birthDate',
        'phone',
        'email',
        'address',
        'role',
        'startDate',
        'status',
        'salary',
        'branch',
        'image',
      ],
      where: { email },
      relations: ['role', 'branch'],
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = new User();
    user.name = updateUserDto.name;
    user.gender = updateUserDto.gender;
    user.height = parseFloat(updateUserDto.height);
    user.weight = parseFloat(updateUserDto.weight);
    user.bloodType = updateUserDto.bloodType;
    user.age = parseInt(updateUserDto.age);
    user.birthDate = new Date(updateUserDto.birthDate);
    user.phone = updateUserDto.phone;
    user.email = updateUserDto.email;
    user.address = updateUserDto.address;
    user.role = JSON.parse(updateUserDto.role);
    user.startDate = new Date(updateUserDto.startDate);
    user.status = updateUserDto.status;
    user.salary = parseFloat(updateUserDto.salary);
    user.branch = JSON.parse(updateUserDto.branch);
    if (updateUserDto.image && updateUserDto.image !== '') {
      user.image = updateUserDto.image;
    }

    const updateUser = await this.usersRepository.findOneOrFail({
      where: { id },
      relations: ['role', 'branch'],
    });

    updateUser.name = user.name;
    updateUser.gender = user.gender;
    updateUser.height = user.height;
    updateUser.weight = user.weight;
    updateUser.bloodType = user.bloodType;
    updateUser.age = user.age;
    updateUser.birthDate = user.birthDate;
    updateUser.phone = user.phone;
    updateUser.email = user.email;
    updateUser.address = user.address;
    updateUser.role = user.role;
    updateUser.startDate = user.startDate;
    updateUser.status = user.status;
    updateUser.salary = user.salary;
    updateUser.branch = user.branch;
    if (user.image && user.image !== '') {
      if (updateUser.image && updateUser.image !== 'noimage.jpg') {
        try {
          await unlink(`./public/images/users/${updateUser.image}`);
        } catch (error) {
          console.error('Image Erorr deleting', error);
        }
      }
      updateUser.image = user.image;
    }
    await this.usersRepository.save(updateUser);
    const result = await this.usersRepository.findOne({
      where: { id },
      relations: ['role', 'branch'],
    });
    return result;
  }

  async remove(id: number) {
    const deleteUser = await this.usersRepository.findOneOrFail({
      where: { id },
    });
    //Delete image file
    if (deleteUser.image) {
      try {
        await unlink(`./public/images/users/${deleteUser.image}`);
      } catch (error) {
        console.error('Image Erorr deleting', error);
      }
    }
    await this.usersRepository.remove(deleteUser);
    return deleteUser;
  }
  async findByFilter(
    role: string,
    gender: string,
    branch: string,
    status: string,
    min_salary: number,
    max_salary: number,
    min_age: number,
    max_age: number,
  ) {
    try {
      const result = await this.usersRepository.query(`CALL getByBrachandfilter(?, ?, ?, ?, ?, ?, ?, ?)`,
        [role, gender, branch, status, min_salary, max_salary, min_age, max_age]);
      const A = result[0];
      return A;
    } catch (error) {
      console.error("Error executing findByFilter:", error);
      throw error;
    }
  }

  async getsale(id: number, by: string) {
    try {
      let query;
      let params;

      if (by === 'branch') {
        query = `
                SELECT 
                    SUM(CASE WHEN DATE(r.createdDate) = DATE(CURRENT_TIMESTAMP) THEN r.RECEIPT_TOTALPRICE ELSE 0 END) AS day,
                    SUM(CASE WHEN EXTRACT(YEAR_MONTH FROM r.createdDate) = EXTRACT(YEAR_MONTH FROM DATE(CURRENT_TIMESTAMP)) THEN r.RECEIPT_TOTALPRICE ELSE 0 END) AS month,
                    SUM(CASE WHEN YEAR(r.createdDate) = YEAR(DATE(CURRENT_TIMESTAMP)) THEN r.RECEIPT_TOTALPRICE ELSE 0 END) AS year
                FROM 
                    receipt r
                WHERE 
                    r.branchId = ?;
            `;
        params = [id];
      } else if (by === 'emp') {
        query = `
                SELECT 
                    SUM(CASE WHEN DATE(r.createdDate) = DATE(CURRENT_TIMESTAMP) THEN r.RECEIPT_TOTALPRICE ELSE 0 END) AS day,
                    SUM(CASE WHEN EXTRACT(YEAR_MONTH FROM r.createdDate) = EXTRACT(YEAR_MONTH FROM DATE(CURRENT_TIMESTAMP)) THEN r.RECEIPT_TOTALPRICE ELSE 0 END) AS month,
                    SUM(CASE WHEN YEAR(r.createdDate) = YEAR(DATE(CURRENT_TIMESTAMP)) THEN r.RECEIPT_TOTALPRICE ELSE 0 END) AS year
                FROM 
                    receipt r
                WHERE 
                    r.userId = ?;
            `;
        params = [id];
      } else if (by === 'all') {
        query = `
                SELECT 
                    SUM(CASE WHEN DATE(r.createdDate) = DATE(CURRENT_TIMESTAMP) THEN r.RECEIPT_TOTALPRICE ELSE 0 END) AS day,
                    SUM(CASE WHEN EXTRACT(YEAR_MONTH FROM r.createdDate) = EXTRACT(YEAR_MONTH FROM DATE(CURRENT_TIMESTAMP)) THEN r.RECEIPT_TOTALPRICE ELSE 0 END) AS month,
                    SUM(CASE WHEN YEAR(r.createdDate) = YEAR(DATE(CURRENT_TIMESTAMP)) THEN r.RECEIPT_TOTALPRICE ELSE 0 END) AS year
                FROM 
                    receipt r;
            `;
        params = [];
      } else {
        throw new Error('Invalid "by" parameter');
      }

      const result = await this.usersRepository.query(query, params);
      return result[0];
    } catch (error) {
      console.error("Error executing findByFilter:", error);
      throw error;
    }
  }

  async getsaleItemDate(id: number, by: string) {
    try {
      let query;
      let params;

      if (by === 'branch') {
        query = `
              SELECT 
                  r.createdDate,
                  rd.RECEIPT_DT_NAME,
                  rd.RECEIPT_DT_QTY,
                  rd.RECEIPT_DT_PRICE,
                  rd.RECEIPT_DT_TOTAL
              FROM 
                  receipt_detail rd
              INNER JOIN 
                  receipt r ON rd.receiptId = r.RECEIPT_ID
              WHERE 
                  r.branchId = ?
                  AND DATE(r.createdDate) = DATE(CURRENT_TIMESTAMP)
              ORDER BY 
                  r.createdDate DESC;
          `;
        params = [id];
      } else if (by === 'emp') {
        query = `
              SELECT 
                  r.createdDate,
                  rd.RECEIPT_DT_NAME,
                  rd.RECEIPT_DT_QTY,
                  rd.RECEIPT_DT_PRICE,
                  rd.RECEIPT_DT_TOTAL
              FROM 
                  receipt_detail rd
              INNER JOIN 
                  receipt r ON rd.receiptId = r.RECEIPT_ID
              WHERE 
                  r.userId = ?
                  AND DATE(r.createdDate) = DATE(CURRENT_TIMESTAMP)
              ORDER BY 
                  r.createdDate DESC;
          `;
        params = [id];
      } else if (by === 'all') {
        query = `
              SELECT 
                  r.createdDate,
                  rd.RECEIPT_DT_NAME,
                  rd.RECEIPT_DT_QTY,
                  rd.RECEIPT_DT_PRICE,
                  rd.RECEIPT_DT_TOTAL
              FROM 
                  receipt_detail rd
              INNER JOIN 
                  receipt r ON rd.receiptId = r.RECEIPT_ID
              WHERE 
                   DATE(r.createdDate) = DATE(CURRENT_TIMESTAMP)
              ORDER BY 
                  r.createdDate DESC;
          `;
        params = [id];
      } else {
        throw new Error('Invalid "by" parameter');
      }

      const result = await this.usersRepository.query(query, params);
      return result;
    } catch (error) {
      console.error("Error executing getsaleItemDate:", error);
      throw error;
    }
  }
  async getsaleItemMonth(id: number, by: string) {
    try {
      let query;
      let params;

      if (by === 'branch') {
        query = `
              SELECT 
                  DATE(r.createdDate) AS Receipt_Date,
                  SUM(r.RECEIPT_QTY) AS Total_Quantity,
                  SUM(r.RECEIPT_TOTALPRICE) AS Total_Price
              FROM 
                  receipt r
              WHERE 
                  r.branchId = ?
                  AND EXTRACT(YEAR_MONTH FROM r.createdDate) = EXTRACT(YEAR_MONTH FROM DATE(CURRENT_TIMESTAMP))
              GROUP BY 
                  DATE(r.createdDate)
              ORDER BY 
                  Receipt_Date DESC;
          `;
        params = [id];
      } else if (by === 'emp') {
        query = `
              SELECT 
                  DATE(r.createdDate) AS Receipt_Date,
                  SUM(r.RECEIPT_QTY) AS Total_Quantity,
                  SUM(r.RECEIPT_TOTALPRICE) AS Total_Price
              FROM 
                  receipt r
              WHERE 
                  r.userId = ?
                  AND EXTRACT(YEAR_MONTH FROM r.createdDate) = EXTRACT(YEAR_MONTH FROM DATE(CURRENT_TIMESTAMP))
              GROUP BY 
                  DATE(r.createdDate)
              ORDER BY 
                  Receipt_Date DESC;
          `;
        params = [id];
      } else if (by === 'all') {
        query = `
              SELECT 
                  DATE(r.createdDate) AS Receipt_Date,
                  SUM(r.RECEIPT_QTY) AS Total_Quantity,
                  SUM(r.RECEIPT_TOTALPRICE) AS Total_Price
              FROM 
                  receipt r
              WHERE 
                  EXTRACT(YEAR_MONTH FROM r.createdDate) = EXTRACT(YEAR_MONTH FROM DATE(CURRENT_TIMESTAMP))
              GROUP BY 
                  DATE(r.createdDate)
              ORDER BY 
                  Receipt_Date DESC;
          `;
        params = [id];
      } else {
        throw new Error('Invalid "by" parameter');
      }

      const result = await this.usersRepository.query(query, params);
      return result;
    } catch (error) {
      console.error("Error executing getsaleItemMonth:", error);
      throw error;
    }
  }

  async getsaleItemYear(id: number, by: string) {
    try {
      let query;
      let params;

      if (by === 'branch') {
        query = `
              SELECT 
                  DATE_FORMAT(r.createdDate, '%Y-%m') AS Receipt_Month,
                  SUM(r.RECEIPT_QTY) AS Total_Quantity,
                  SUM(r.RECEIPT_TOTALPRICE) AS Total_Price
              FROM 
                  receipt r
              WHERE 
                  r.branchId = ?
                  AND EXTRACT(YEAR_MONTH FROM r.createdDate) = EXTRACT(YEAR_MONTH FROM DATE(CURRENT_TIMESTAMP))
              GROUP BY 
                  DATE_FORMAT(r.createdDate, '%Y-%m')
              ORDER BY 
                  Receipt_Month DESC;
          `;
        params = [id];
      } else if (by === 'emp') {
        query = `
              SELECT 
                  DATE_FORMAT(r.createdDate, '%Y-%m') AS Receipt_Month,
                  SUM(r.RECEIPT_QTY) AS Total_Quantity,
                  SUM(r.RECEIPT_TOTALPRICE) AS Total_Price
              FROM 
                  receipt r
              WHERE 
                  r.userId = ?
                  AND EXTRACT(YEAR_MONTH FROM r.createdDate) = EXTRACT(YEAR_MONTH FROM DATE(CURRENT_TIMESTAMP))
              GROUP BY 
                  DATE_FORMAT(r.createdDate, '%Y-%m')
              ORDER BY 
                  Receipt_Month DESC;
          `;
        params = [id];
      } else if (by === 'all') {
        query = `
              SELECT 
                  DATE_FORMAT(r.createdDate, '%Y-%m') AS Receipt_Month,
                  SUM(r.RECEIPT_QTY) AS Total_Quantity,
                  SUM(r.RECEIPT_TOTALPRICE) AS Total_Price
              FROM 
                  receipt r
              WHERE 
                  EXTRACT(YEAR_MONTH FROM r.createdDate) = EXTRACT(YEAR_MONTH FROM DATE(CURRENT_TIMESTAMP))
              GROUP BY 
                  DATE_FORMAT(r.createdDate, '%Y-%m')
              ORDER BY 
                  Receipt_Month DESC;
          `;
        params = [id];
      } else {
        throw new Error('Invalid "by" parameter');
      }

      const result = await this.usersRepository.query(query, params);
      return result;
    } catch (error) {
      console.error("Error executing getsaleItemYear:", error);
      throw error;
    }
  }

  async CalculateSalary(id: number) {
    try {
      console.log('1')
      const result = await this.usersRepository.query(`CALL CalculateSalary(?)`, [id])
      return result[0]
    } catch (e: any) {
      console.error("Error executing CalculateSalary:");
    }
  }



}
