//import { Stock } from 'src/stocks/entities/stock.entity';
import { Role } from '../../roles/entities/role.entity';
import { Branch } from '../../branches/entities/branch.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  OneToMany,
  // ManyToMany,
} from 'typeorm';
import { CheckInOut } from 'src/check-in-out/entities/check-in-out.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import { Stock } from 'src/stocks/entities/stock.entity';
import { UtilityCost } from 'src/utility-cost/entities/utility-cost.entity';
import { Salary } from 'src/salarys/entities/salary.entity';
//import { UtilityCost } from 'src/utility-cost/entities/utility-cost.entity';

@Entity({
  name: 'employee',
  engine: 'InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci',
})
export class User {
  // ID
  @PrimaryGeneratedColumn({
    name: 'EMP_ID',
  })
  id: number;

  // Information about employee
  @Column({
    name: 'EMP_NAME',
  })
  name: string;
  @Column({
    name: 'EMP_GENDER',
  })
  gender: string;
  @Column({
    name: 'EMP_HEIGHT', //by Centimeter
    type: 'decimal',
    precision: 10,
    scale: 2,
  })
  height: number;
  @Column({
    name: 'EMP_WEIGHT', //by Kilogram
    type: 'decimal',
    precision: 10,
    scale: 2,
  })
  weight: number;
  @Column({
    name: 'EMP_BLOODTYPE',
  })
  bloodType: string;

  @Column({
    name: 'EMP_AGE',
  })
  age: number;

  @Column({
    name: 'EMP_BIRTHDATE',
    type: 'date',
  })
  birthDate: Date;

  //Contact Information
  @Column({
    name: 'EMP_PHONE',
  })
  phone: string;

  @Column({
    name: 'EMP_EMAIL',
    unique: true,
  })
  email: string;

  @Column({
    name: 'EMP_ADDRESS',
  })
  address: string;

  //Work Information
  @ManyToOne(() => Role, (role) => role.users)
  @JoinColumn({
    name: 'ROLE_ID',
  })
  role: Role;

  @Column({
    name: 'EMP_STARTDATE',
    type: 'date',
  })
  startDate: Date;

  @Column({
    name: 'EMP_STATUS',
  })
  status: string;
  @Column({
    name: 'EMP_SALARY',
    type: 'decimal',
    precision: 10,
    scale: 2,
  })
  salary: number;

  @ManyToOne(() => Branch, (branch) => branch.users)
  @JoinColumn({ name: 'BRANCH_ID' })
  branch: Branch;

  //Photo
  @Column({
    name: 'EMP_PHOTO',
    default: 'noimage.jpg',
  })
  image: string;

  //Created and Updated
  @CreateDateColumn({
    name: 'EMP_CREATED',
  })
  created: Date;

  @UpdateDateColumn({
    name: 'EMP_UPDATED',
  })
  updated: Date;

  //For Login
  //Use Email
  @Column({
    select: false,
    name: 'EMP_PASSWORD',
  })
  password: string;

  //Relationship
  @OneToMany(() => Receipt, (receipt) => receipt.user)
  receipts: Receipt[];

  @OneToMany(() => Stock, (stock) => stock.S_USER)
  stocks: Stock[];

  // @OneToMany(() => UtilityCost, (utilityCost) => utilityCost.user)
  // utilityCosts: UtilityCost[];

  @OneToMany(() => CheckInOut, (checkInOut) => checkInOut.user)
  checkInOut: CheckInOut;

  @OneToMany(() => UtilityCost, (utilityCost) => utilityCost.user)
  utilityCosts: UtilityCost[];

  @OneToMany(() => Salary, (salary) => salary.user)
  salarys: Salary[];
}
