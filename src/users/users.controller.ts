import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { AuthGuard } from 'src/auth/auth.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { uuid } from 'uuidv4';
import { extname } from 'path';

@UseGuards(AuthGuard)
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) { }

  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/users',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  create(
    @Body() createUserDto: CreateUserDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file && file.filename) {
      createUserDto.image = file.filename;
    }
    return this.usersService.create(createUserDto);
  }

  @Patch('changepassword')
  chagePassword(
    @Body('email') email: string,
    @Body('password') password: string,
  ) {
    return this.usersService.chagePassword(email, password);
  }

  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @Get('role/:roleId')
  findAllByRole(@Param('roleId') roleId: string) {
    return this.usersService.findAllByRole(+roleId);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(+id);
  }

  @Get('email/:email')
  findOneByEmail(@Param('email') email: string) {
    return this.usersService.findOneByEmail(email);
  }

  @Get('branch/:branchId')
  findAllByType(@Param('branchId') branchId: string) {
    return this.usersService.findByBranch(+branchId);
  }

  @Patch(':id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/users',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file && file.filename) {
      updateUserDto.image = file.filename;
    }
    return this.usersService.update(+id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }

  @Post('upload')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/users',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  uploadFile(
    @Body() user: { name: string; age: number },
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log(user);
    console.log(file.filename);
    console.log(file.path);
  }
  @Get('filter/:role/:gender/:branch/:status/:min_salary/:max_salary/:min_age/:max_age')
  async findAllByFilter(
    @Param('role') role: string,
    @Param('gender') gender: string,
    @Param('branch') branch: string,
    @Param('status') status: string,
    @Param('min_salary') min_salary: number,
    @Param('max_salary') max_salary: number,
    @Param('min_age') min_age: number,
    @Param('max_age') max_age: number,
  ) {
    try {
      const result = await this.usersService.findByFilter(
        role,
        gender,
        branch,
        status,
        min_salary,
        max_salary,
        min_age,
        max_age,
      );
      return result;
    } catch (error) {
      console.error('Error executing findByFilter:', error);
      throw error;
    }
  }

  @Get('getpr0duct/:by/ByID/:id')
getSaleProducts(
  @Param('id') id: number,
  @Param('by') by: string
) {
  return this.usersService.getsale(id, by);
}

@Get('getsaleItemDate/:by/ByID/:id')
getSaleItemDate(
  @Param('id') id: number,
  @Param('by') by: string
) {
  return this.usersService.getsaleItemDate(id, by);
}

@Get('getsaleItemMonth/:by/ByID/:id')
getSaleItemMonth(
  @Param('id') id: number,
  @Param('by') by: string
) {
  return this.usersService.getsaleItemMonth(id, by);
}

@Get('getsaleItemYear/:by/ByID/:id')
getSaleItemYear(
  @Param('id') id: number,
  @Param('by') by: string
) {
  return this.usersService.getsaleItemYear(id, by);
}



  @Post('/users/:id')
  async CalculateSalary(@Param('id') id: number) {
    try {
      const result = await this.usersService.CalculateSalary(id);
      return result
    } catch (e: any) {
      console.log('error')
    }
  }

}
