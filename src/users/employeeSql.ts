import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class EmployeeSql {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) { }

  async createGetByBrachandfilterProcedure() {
    try {
      // Check if the procedure exists
      const existingProcedure = await this.userRepository.query(
        `SHOW PROCEDURE STATUS WHERE Name = 'getByBrachandfilter';`,
      );
      // If the procedure exists, drop it
      if (existingProcedure.length > 0) {
        await this.userRepository.query(`DROP PROCEDURE getByBrachandfilter;`);
        console.log('Dropped getByBrachandfilter procedure');
      } else {
        console.log('No getByBrachandfilter procedure found to drop');
      }
      // Create the new procedure
      await this.userRepository.query(`
        CREATE PROCEDURE getByBrachandfilter(
            IN role_param VARCHAR(255), 
            IN sex_param VARCHAR(255),
            IN branch_param VARCHAR(255),
            IN status_param VARCHAR(255),
            IN min_salary_param INT,
            IN max_salary_param INT,
            IN min_age_param INT, 
            IN max_age_param INT
        )
        BEGIN
            SELECT 
                employee.EMP_ID, employee.EMP_NAME, employee.EMP_GENDER, employee.EMP_AGE, employee.EMP_BIRTHDATE, employee.EMP_PHONE,
                employee.EMP_SALARY, employee.EMP_PHOTO, employee.EMP_STATUS, employee.EMP_STARTDATE, employee.EMP_CREATED,
                employee.EMP_UPDATED, branch.BRANCH_ID, branch.BRANCH_ADDRESS, branch.BRANCH_CITY,
                branch.BRANCH_PROVINCE, branch.BRANCH_COUNTRY, branch.BRANCH_POSTAL_CODE, branch.BRANCH_CREATED,
                branch.BRANCH_UPDATED, branch.BRANCH_NAME, role.ROLE_ID, role.ROLE_NAME,
                (SELECT COUNT(*) FROM employee 
                 WHERE (sex_param = 'all' OR employee.EMP_GENDER = sex_param) 
                   AND (role_param = 'all' OR role.ROLE_NAME = role_param) 
                   AND (branch_param = 'all' OR branch.BRANCH_NAME = branch_param)
                   AND (status_param = 'all' OR employee.EMP_STATUS = status_param)
                   AND (min_salary_param <= employee.EMP_SALARY AND max_salary_param >= employee.EMP_SALARY)
                   AND employee.EMP_AGE BETWEEN min_age_param AND max_age_param) AS EMPLOYEE_COUNT
            FROM employee
            INNER JOIN branch ON employee.BRANCH_ID = branch.BRANCH_ID
            LEFT JOIN role ON employee.ROLE_ID = role.ROLE_ID
            WHERE (sex_param = 'all' OR employee.EMP_GENDER = sex_param) 
              AND (role_param = 'all' OR role.ROLE_NAME = role_param) 
              AND (branch_param = 'all' OR branch.BRANCH_NAME = branch_param)
              AND (status_param = 'all' OR employee.EMP_STATUS = status_param)
              AND (min_salary_param <= employee.EMP_SALARY AND max_salary_param >= employee.EMP_SALARY)
              AND (min_age_param <= employee.EMP_AGE AND max_age_param >= employee.EMP_AGE)
        ;
        END;
    `);
      console.log("Created getByBrachandfilter procedure successfully");
    } catch (error) {
      throw new Error('Failed to create getByBrachandfilter procedure');
    }
  }
}
