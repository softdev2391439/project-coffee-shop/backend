import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Role } from 'src/roles/entities/role.entity';
import { Branch } from '../branches/entities/branch.entity';
import { EmployeeSql } from './employeeSql';

@Module({
  imports: [TypeOrmModule.forFeature([User, Role, Branch])],
  controllers: [UsersController],
  providers: [UsersService, EmployeeSql],
  exports: [UsersService],
})
export class UsersModule {
  constructor(private sqlRunner: EmployeeSql) {
    this.sqlRunner.createGetByBrachandfilterProcedure();
  }
}
