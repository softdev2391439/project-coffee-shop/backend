import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ engine: 'InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci' })
export class Material {
  @PrimaryGeneratedColumn()
  M_ID: number;

  @Column()
  M_NAME: string;

  @Column()
  M_MIN: number;

  @Column()
  M_AMOUNT: number;

  @Column()
  M_TYPE: string;
}
