import { Module } from '@nestjs/common';
import { BranchesService } from './branches.service';
import { BranchesController } from './branches.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Branch } from './entities/branch.entity';
import { Salary } from 'src/salarys/entities/salary.entity';
import { BranchSql } from './branchSql';

@Module({
  imports: [TypeOrmModule.forFeature([Branch, Salary])],
  controllers: [BranchesController],
  providers: [BranchesService, BranchSql],
})
export class BranchesModule {
  constructor(private branchSql: BranchSql) {
    this.branchSql.createGetBranchesProcedure();
    this.branchSql.createGetBranchByIdProcedure();
  }
}
