import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Branch } from './entities/branch.entity';
import { Repository } from 'typeorm';

@Injectable()
export class BranchSql {
  constructor(
    @InjectRepository(Branch)
    private readonly branchRepository: Repository<Branch>,
  ) {}

  async createGetBranchesProcedure() {
    try {
      // Check if the procedure exists
      const existingProcedure = await this.branchRepository.query(
        `SHOW PROCEDURE STATUS WHERE Name = 'getBranches';`,
      );
      // If the procedure exists, drop it
      if (existingProcedure.length > 0) {
        await this.branchRepository.query(`DROP PROCEDURE getBranches;`);
        console.log('Dropped getBranches procedure');
      } else {
        console.log('No getBranches procedure found to drop');
      }
      // Create the new procedure
      await this.branchRepository.query(`
          CREATE PROCEDURE getBranches()
          BEGIN
              SELECT * FROM branch;
          END;
      `);
      console.log('Created getBranches procedure successfully');
    } catch (error) {
      throw new Error('Failed to create getBranches procedure');
    }
  }

  async createGetBranchByIdProcedure() {
    try {
      // Check if the procedure exists
      const existingProcedure = await this.branchRepository.query(
        `SHOW PROCEDURE STATUS WHERE Name = 'getBranchById';`,
      );
      // If the procedure exists, drop it
      if (existingProcedure.length > 0) {
        await this.branchRepository.query(`DROP PROCEDURE getBranchById;`);
        console.log('Dropped getBranchById procedure');
      } else {
        console.log('No getBranchById procedure found to drop');
      }
      // Create the new procedure
      await this.branchRepository.query(`
          CREATE PROCEDURE getBranchById (IN pBRANCH_ID INT)
          BEGIN
              SELECT *
              FROM branch
              WHERE BRANCH_ID = pBRANCH_ID;
          END;
      `);
      console.log('Created getBranchById procedure successfully');
    } catch (error) {
      throw new Error('Failed to create getBranchById procedure');
    }
  }
}
