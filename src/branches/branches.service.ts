import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateBranchDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { Repository } from 'typeorm';
import { Branch } from './entities/branch.entity';

@Injectable()
export class BranchesService {
  constructor(
    @InjectRepository(Branch) private branchesRepository: Repository<Branch>,
  ) {}
  async create(createBranchDto: CreateBranchDto) {
    return await this.branchesRepository.save(createBranchDto);
  }

  async findAll() {
    const result = await this.branchesRepository.query('CALL getBranches()');
    if (!result || result.length === 0 || result[0].length === 0) {
      throw new Error('No branch found with the provided ID');
    }
    const branchData = result[0];
    return branchData;
  }

  async findOne(id) {
    try {
      const result = await this.branchesRepository.query(
        'CALL getBranchById(' + id + ')',
      );
      if (!result || result.length === 0 || result[0].length === 0) {
        throw new Error('No branch found with the provided ID');
      }
      const branchData = result[0][0];
      return branchData;
    } catch (error) {
      throw new Error('Error while fetching branch details: ' + error.message);
    }
  }

  async update(id: number, updateBranchDto: UpdateBranchDto) {
    await this.branchesRepository.findOneByOrFail({ id });
    await this.branchesRepository.update(id, updateBranchDto);
    return await this.branchesRepository.findOneByOrFail({ id });
  }

  async remove(id: number) {
    const removeBranch = await this.branchesRepository.findOneByOrFail({ id });
    await this.branchesRepository.remove(removeBranch);
    return removeBranch;
  }
}
